resource "azurerm_resource_group" "res-0" {
  location   = "francecentral"
  managed_by = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourcegroups/OCC_ASD_loutre_bourree/providers/Microsoft.ContainerService/managedClusters/AKS_LB"
  name       = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  tags = {
    aks-managed-cluster-name = "AKS_LB"
    aks-managed-cluster-rg   = "OCC_ASD_loutre_bourree"
  }
}
resource "azurerm_linux_virtual_machine_scale_set" "res-1" {
  admin_username         = "azureuser"
  extensions_time_budget = "PT16M"
  instances              = 1
  location               = "francecentral"
  name                   = "aks-nodepool1-35950065-vmss"
  overprovision          = false
  resource_group_name    = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  single_placement_group = false
  sku                    = "Standard_DS2_v2"
  source_image_id        = "/subscriptions/109a5e88-712a-48ae-9078-9ca8b3c81345/resourceGroups/AKS-Ubuntu/providers/Microsoft.Compute/galleries/AKSUbuntu/images/2204gen2containerd/versions/202405.03.0"
  tags = {
    aks-managed-consolidated-additional-properties = "3f5620da-1daf-11ef-9cea-3245cbc70b6c"
    aks-managed-coordination                       = "true"
    aks-managed-createOperationID                  = "816984c3-c0c1-410d-abcc-265301f1ca4b"
    aks-managed-creationSource                     = "vmssclient-aks-nodepool1-35950065-vmss"
    aks-managed-kubeletIdentityClientID            = "aadc54f1-b857-4b5f-858b-5eee281b3bac"
    aks-managed-orchestrator                       = "Kubernetes:1.28.9"
    aks-managed-poolName                           = "nodepool1"
    aks-managed-resourceNameSuffix                 = "31248105"
    aks-managed-ssh-access                         = "LocalUser"
  }
  admin_ssh_key {
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD2Q6o0tXp/Y+9pXFh9SM8ucoDG8yxB/1kV8ci9EY30lpiUTbXB0rCVDLaOr5yWch1++4nZ781Qd6mBbery/HjGphs83ynnhCzfbZz8rRHU16h8j5Kg03L6NYjD+JWazyNu1msW2+yxdsSb52AgI7yrUnJfx3nP5Fjmh+8DJ9kyIkBv1zGH3dsDNHLFbVXt+oJGTa3LWjIxmBt2knVyIpvJ79lflUsam7FLEq1YgYYr9IXeO5ZFaHOW4rMpp1C+Fg0F2EMJv/CAwdAHuzPrmuwKl6mGLS0p6oRhdG1NJRL6ITIEsgh4woucKUONRdA2MPjF924uO/sDxXbKY8alYcGD"
    username   = "azureuser"
  }
  identity {
    identity_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.ManagedIdentity/userAssignedIdentities/AKS_LB-agentpool"]
    type         = "UserAssigned"
  }
  network_interface {
    enable_accelerated_networking = true
    enable_ip_forwarding          = true
    name                          = "aks-nodepool1-35950065-vmss"
    primary                       = true
    ip_configuration {
      load_balancer_backend_address_pool_ids = ["/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/loadBalancers/kubernetes/backendAddressPools/aksOutboundBackendPool", "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/loadBalancers/kubernetes/backendAddressPools/kubernetes"]
      name                                   = "ipconfig1"
      primary                                = true
      subnet_id                              = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/virtualNetworks/aks-vnet-31248105/subnets/aks-subnet"
    }
  }
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }
  depends_on = [
    azurerm_user_assigned_identity.res-9,
    azurerm_lb_backend_address_pool.res-11,
    azurerm_lb_backend_address_pool.res-12,
    # One of azurerm_subnet.res-16,azurerm_subnet_network_security_group_association.res-17,azurerm_subnet_route_table_association.res-18 (can't auto-resolve as their ids are identical)
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-2" {
  auto_upgrade_minor_version = false
  name                       = "AKSLinuxExtension"
  publisher                  = "Microsoft.AKS"
  settings = jsonencode({
    disable-uu        = "true"
    enable-uu         = "false"
    node-exporter-tls = "false"
  })
  type                         = "Compute.AKS.Linux.AKSNode"
  type_handler_version         = "1.130"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Compute/virtualMachineScaleSets/aks-nodepool1-35950065-vmss"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-1,
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-3" {
  name                         = "aks-nodepool1-35950065-vmss-AKSLinuxBilling"
  publisher                    = "Microsoft.AKS"
  type                         = "Compute.AKS.Linux.Billing"
  type_handler_version         = "1.0"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Compute/virtualMachineScaleSets/aks-nodepool1-35950065-vmss"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-1,
  ]
}
resource "azurerm_virtual_machine_scale_set_extension" "res-4" {
  name                         = "vmssCSE"
  publisher                    = "Microsoft.Azure.Extensions"
  type                         = "CustomScript"
  type_handler_version         = "2.0"
  virtual_machine_scale_set_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Compute/virtualMachineScaleSets/aks-nodepool1-35950065-vmss"
  depends_on = [
    azurerm_linux_virtual_machine_scale_set.res-1,
  ]
}
resource "azurerm_user_assigned_identity" "res-9" {
  location            = "francecentral"
  name                = "AKS_LB-agentpool"
  resource_group_name = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_lb" "res-10" {
  location            = "francecentral"
  name                = "kubernetes"
  resource_group_name = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  sku                 = "Standard"
  tags = {
    aks-managed-cluster-name = "AKS_LB"
    aks-managed-cluster-rg   = "OCC_ASD_loutre_bourree"
  }
  frontend_ip_configuration {
    name = "279bf209-47cb-4138-9fa5-b3050a76b56d"
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_lb_backend_address_pool" "res-11" {
  loadbalancer_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/loadBalancers/kubernetes"
  name            = "aksOutboundBackendPool"
  depends_on = [
    azurerm_lb.res-10,
  ]
}
resource "azurerm_lb_backend_address_pool" "res-12" {
  loadbalancer_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/loadBalancers/kubernetes"
  name            = "kubernetes"
  depends_on = [
    azurerm_lb.res-10,
  ]
}
resource "azurerm_network_security_group" "res-13" {
  location            = "francecentral"
  name                = "aks-agentpool-31248105-nsg"
  resource_group_name = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_public_ip" "res-14" {
  allocation_method   = "Static"
  location            = "francecentral"
  name                = "279bf209-47cb-4138-9fa5-b3050a76b56d"
  resource_group_name = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  sku                 = "Standard"
  tags = {
    aks-managed-cluster-name = "AKS_LB"
    aks-managed-cluster-rg   = "OCC_ASD_loutre_bourree"
    aks-managed-type         = "aks-slb-managed-outbound-ip"
  }
  zones = ["1", "2", "3"]
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_virtual_network" "res-15" {
  address_space       = ["10.224.0.0/12"]
  location            = "francecentral"
  name                = "aks-vnet-31248105"
  resource_group_name = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_subnet" "res-16" {
  address_prefixes     = ["10.224.0.0/16"]
  name                 = "aks-subnet"
  resource_group_name  = "MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral"
  virtual_network_name = "aks-vnet-31248105"
  depends_on = [
    azurerm_virtual_network.res-15,
  ]
}
resource "azurerm_subnet_network_security_group_association" "res-17" {
  network_security_group_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/networkSecurityGroups/aks-agentpool-31248105-nsg"
  subnet_id                 = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/virtualNetworks/aks-vnet-31248105/subnets/aks-subnet"
  depends_on = [
    azurerm_network_security_group.res-13,
    # One of azurerm_subnet.res-16,azurerm_subnet_route_table_association.res-18 (can't auto-resolve as their ids are identical)
  ]
}
resource "azurerm_subnet_route_table_association" "res-18" {
  route_table_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/routeTables/aks-agentpool-31248105-routetable"
  subnet_id      = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/MC_OCC_ASD_loutre_bourree_AKS_LB_francecentral/providers/Microsoft.Network/virtualNetworks/aks-vnet-31248105/subnets/aks-subnet"
  depends_on = [
    # One of azurerm_subnet.res-16,azurerm_subnet_network_security_group_association.res-17 (can't auto-resolve as their ids are identical)
  ]
}
resource "azurerm_route_table" "res-19" {
  location            = "francecentral"
  name                = "aks-agentpool-31248105-routetable"
  resource_group_name = "mc_occ_asd_loutre_bourree_aks_lb_francecentral"
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_route" "res-20" {
  address_prefix         = "10.244.0.0/24"
  name                   = "aks-nodepool1-35950065-vmss000000____102440024"
  next_hop_in_ip_address = "10.224.0.4"
  next_hop_type          = "VirtualAppliance"
  resource_group_name    = "mc_occ_asd_loutre_bourree_aks_lb_francecentral"
  route_table_name       = "aks-agentpool-31248105-routetable"
  depends_on = [
    azurerm_route_table.res-19,
  ]
}
