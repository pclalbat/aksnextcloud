resource "azurerm_resource_group" "res-0" {
  location = "francecentral"
  name     = "OCC_ASD_loutre_bourree"
}
resource "azurerm_kubernetes_cluster" "res-1" {
  dns_prefix          = "AKSLB-OCCASDloutrebour-0b15b4"
  location            = "francecentral"
  name                = "AKS_LB"
  resource_group_name = "OCC_ASD_loutre_bourree"
  default_node_pool {
    name    = "nodepool1"
    vm_size = "Standard_DS2_v2"
    upgrade_settings {
      max_surge = "10%"
    }
  }
  identity {
    type = "SystemAssigned"
  }
  linux_profile {
    admin_username = "azureuser"
    ssh_key {
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD2Q6o0tXp/Y+9pXFh9SM8ucoDG8yxB/1kV8ci9EY30lpiUTbXB0rCVDLaOr5yWch1++4nZ781Qd6mBbery/HjGphs83ynnhCzfbZz8rRHU16h8j5Kg03L6NYjD+JWazyNu1msW2+yxdsSb52AgI7yrUnJfx3nP5Fjmh+8DJ9kyIkBv1zGH3dsDNHLFbVXt+oJGTa3LWjIxmBt2knVyIpvJ79lflUsam7FLEq1YgYYr9IXeO5ZFaHOW4rMpp1C+Fg0F2EMJv/CAwdAHuzPrmuwKl6mGLS0p6oRhdG1NJRL6ITIEsgh4woucKUONRdA2MPjF924uO/sDxXbKY8alYcGD"
    }
  }
  depends_on = [
    azurerm_resource_group.res-0,
  ]
}
resource "azurerm_kubernetes_cluster_node_pool" "res-2" {
  kubernetes_cluster_id = "/subscriptions/0b15b40d-18c9-4474-8fa0-26b43d03ba8c/resourceGroups/OCC_ASD_loutre_bourree/providers/Microsoft.ContainerService/managedClusters/AKS_LB"
  mode                  = "System"
  name                  = "nodepool1"
  vm_size               = "Standard_DS2_v2"
  upgrade_settings {
    max_surge = "10%"
  }
  depends_on = [
    azurerm_kubernetes_cluster.res-1,
  ]
}
